Xspice
###############################################################################

:slug: xspice
:modified: 2016-01-05 10:28

.. _Download: download.html

What is it
+++++++++++

A standalone server that is both an X server and a Spice server. Iow, you get a new DISPLAY to launch X clients against, and you can view and interact with them via a spice client.

Status
++++++

Released, now part of xf86-video-qxl. Download_

Known Problems
++++++++++++++

!!This page is not up to date!!

Bugs for Xspice are reported against the xorg-x11-drv-qxl component due to technically the Xspice server being part of the xf86-video-qxl driver tarball, and should all have Xspice in the title: https://bugs.freedesktop.org/enter_bug.cgi?product=xorg

Lock when last X client leaves
++++++++++++++++++++++++++++++

Bug in handling server reset. Not yet fixed. Workaround: run with xspice "-noreset" flag (it is passed to Xorg and does just what it says - no reset when last client leaves. Meaning you have to explicitly kill the Xorg, by Ctrl-C or a signal).

Performance issues
++++++++++++++++++

- takes way too much memory (256MB)
- some stuff is just too damn slow - like lines and circles in gtkperf (and hence in anything else) (this is actually the same in a vm - it's a problem of the driver / device / architecture)
- cursor is only server side (i.e. cursor channel not really used? not sure)
